import segyio
import numpy as np
from scipy.fftpack import fft, ifft

def fftpeak(traces,samplerate):
    fts=np.fft.fft(traces)
    freq = np.fft.fftfreq(len(traces), d=samplerate)
    out=[freq,np.abs(fts)]
    return out

segy_input = r"C:\Users\ketilwa\Documents\Data\100_Poseidon3D_FullStack_2362-2366.segy"
npsavefile = r"C:\Users\ketilwa\Documents\Data\100_Poseidon3D_FullStack_2362-2366.npy"
segy_frq_output = r"C:\Users\ketilwa\Documents\Data\100_Poseidon3D_FullStack_2362-2366_freq.segy"

# Open the input SEGY file

with segyio.open(segy_input) as f:
    text_header = segyio.tools.wrap(f.text[0])
    print(text_header)
    print(f.bin)

    # Number of samples per trace from binary header
    nsamples = f.bin[segyio.BinField.Samples]

    # Get sample rate from the first trace header. Assume it is constant for all traces.
    h1 = f.header[0]
    sr = h1[segyio.TraceField.TRACE_SAMPLE_INTERVAL]/1000

    # Total number of traces to process
    ntraces = f.tracecount

    # All samples as ndarray
    data = f.trace[1:ntraces]

    # Define window length
    ns_per_window = int(500/sr) # 500 ms window
    num_windows = nsamples - ns_per_window

    # ndarray to hold results
    traces_max_freq = np.ndarray((ntraces, nsamples))

    # Loop through all traces
    tno = 0
    for t in data:

        max_frequencies = np.zeros(nsamples)
        shift = int(ns_per_window/2)

        for wn in range(nsamples-shift):
            last_sample = wn + ns_per_window
            if last_sample > nsamples:
                last_sample = nsamples

            wt = t[wn:last_sample] # trace in window
            if len(wt) < ns_per_window:
                wt = np.pad(wt, (0, ns_per_window - len(wt)), 'constant')

            test=fftpeak(wt,sr/1000.0)
            x, y = test[0], test[1]
            maxidx = np.argmax(y)
            maxfreq = x[maxidx]

            max_frequencies[wn+shift] = maxfreq

        traces_max_freq[tno] = max_frequencies
        tno += 1
    
    np.save(npsavefile, traces_max_freq)
    
    spec = segyio.tools.metadata(f)
    with segyio.create(segy_frq_output, spec) as dst:
         dst.text[0] = f.text[0]
         dst.bin = f.bin
         dst.header = f.header
         dst.trace = traces_max_freq

    pass

    
    



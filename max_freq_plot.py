import segyio
import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft, ifft

def fftpeak(traces,samplerate):
    fts=np.fft.fft(traces)
    freq = np.fft.fftfreq(len(traces), d=samplerate)
    out=[freq,np.abs(fts)]
    return out

segy_input = r"C:\Users\ketilwa\Documents\Data\Poseidon3D_FullStack_SubSub.segy"

with segyio.open(segy_input) as f:
    text_header = segyio.tools.wrap(f.text[0])
    print(text_header)
    print(f.bin)
    nsamples = f.bin[segyio.BinField.Samples]
    h1 = f.header[0]
    sr = h1[segyio.TraceField.TRACE_SAMPLE_INTERVAL]/1000

    ntraces = f.tracecount
    if ntraces > 2000:
        ntraces = 2000

    data = f.trace[1:ntraces]
    traces = np.ndarray((ntraces, nsamples))
    ns_per_window = int(500/sr) # 500 ms window
    num_windows = nsamples - ns_per_window

    traces_max_freq = np.ndarray((ntraces, nsamples))

    tno = 0
    for t in data:
        traces[tno] = t

        max_frequencies = np.zeros(nsamples)
        shift = int(ns_per_window/2)

        for wn in range(nsamples-shift):
            last_sample = wn + ns_per_window
            if last_sample > nsamples:
                last_sample = nsamples

            wt = t[wn:last_sample] # trace in window
            if len(wt) < ns_per_window:
                wt = np.pad(wt, (0, ns_per_window - len(wt)), 'constant')
            test=fftpeak(wt,sr/1000.0)
            x, y = test[0], test[1]
            maxidx = np.argmax(y)
            maxfreq = x[maxidx]

            max_frequencies[wn+shift] = maxfreq

        traces_max_freq[tno] = max_frequencies

        tno += 1
    
    print(traces.shape)
    min_amp = traces.min()
    max_amp = traces.max()
    vm = max([abs(min_amp), max_amp])
    vm = np.percentile(traces, 99)
    print('Min {}  max {}  99th perc {} amplitude'.format(traces.min(), traces.max(), vm))

    extent = [1, ntraces, nsamples*sr, 0]

    plt.figure(1)
    plt.imshow(traces.T, cmap="Greys", vmin=-vm, vmax=vm, aspect='auto', extent=extent)
    plt.ylabel('Time')
    plt.xlabel('TraceNo')

    min_freq = traces_max_freq.min()
    max_freq = traces_max_freq.max()

    fextent = [1, ntraces, nsamples*sr, 0]
    plt.figure(2)
    plt.imshow(traces_max_freq.T, cmap="Greys", vmin=min_freq, vmax=70, aspect='auto', extent=fextent)
    plt.ylabel('Time')
    plt.xlabel('TraceNo')

    plt.show()

    pass

    
    


